#from django.shortcuts import render
from django.shortcuts import render , get_object_or_404
from django.http import HttpResponse
from .models import Question , Choice
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
def index(request):
    qlist = Question.objects.all()
    context = {
            "question" : qlist
        }
    return render(request,"nava/index.html", context)

def detail(request , q_id):
    question = get_object_or_404(Question , pk = q_id)
    context = {
            "question" : question
        }
    return render(request,"nava/detail.html", context)

def vote (request , q_id):
     question = get_object_or_404(Question, pk=q_id)
     try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
     except (KeyError, Choice.DoesNotExist):
         return render(request, 'polls/detail.html', {
             'question': question,
             'error_message': "You didn't select a choice.",
        })
     else:
         selected_choice.votes += 1
         selected_choice.save()
         return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
     
def results(request , q_id):
    return render(request , 'nava/results.html')
